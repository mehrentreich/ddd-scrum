/*
 * Copyright 2020 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ehrentreich.scrum.application

import arrow.core.Either
import arrow.core.Right
import net.ehrentreich.scrum.application.SprintDto.Companion.fromDomainAggregate
import net.ehrentreich.scrum.domain.Sprint
import net.ehrentreich.scrum.domain.SprintIdGenerator
import net.ehrentreich.scrum.domain.SprintRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Transactional
interface SprintApplicationService {
    fun createNewSprint(): Either<ApplicationError, UUID>
    fun fetchASingleSprintById(sprintId: UUID): Either<ApplicationError, SprintDto>
}

@Service
open class SprintApplicationServiceBean(private val sprintIdGenerator: SprintIdGenerator,
                                        private val sprintRepository: SprintRepository) : SprintApplicationService {
    override fun createNewSprint(): Either<ApplicationError, UUID> {
        val sprintId = sprintIdGenerator.nextId()
        val sprint = Sprint(sprintId)
        sprintRepository.add(sprint)
        return Right(sprintId)
    }

    override fun fetchASingleSprintById(sprintId: UUID): Either<ApplicationError, SprintDto> =
            sprintRepository
                .findById(sprintId)
                .map(::fromDomainAggregate)
                .toEither { SprintNotFoundError(sprintId) }
}
