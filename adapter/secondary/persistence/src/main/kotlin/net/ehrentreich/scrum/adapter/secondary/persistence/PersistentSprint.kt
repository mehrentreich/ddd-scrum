/*
 * Copyright 2020 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ehrentreich.scrum.adapter.secondary.persistence

import net.ehrentreich.scrum.adapter.secondary.persistence.PersistentSprint.Companion.QueryFindBySprintId
import net.ehrentreich.scrum.domain.Sprint
import org.hibernate.annotations.NaturalId
import java.util.*
import javax.persistence.*

@Suppress("JpaObjectClassSignatureInspection")
@NamedQuery(name = QueryFindBySprintId, query = "select ps from PersistentSprint ps where ps.sprintId = :sprintId")
@Entity
internal data class PersistentSprint(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id
        val id: Long,

        @Column(nullable = false, unique = true)
        @NaturalId
        val sprintId: UUID
) {
    companion object {
        const val QueryFindBySprintId = "QUERY_FIND_BY_SPRINT_ID"

        fun fromDomainAggregate(sprint: Sprint): PersistentSprint = PersistentSprint(0, sprint.sprintId)
    }

    fun toDomainAggregate(): Sprint = Sprint(sprintId)
}
