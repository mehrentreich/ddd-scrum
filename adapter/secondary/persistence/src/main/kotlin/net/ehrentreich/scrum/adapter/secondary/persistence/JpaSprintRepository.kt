/*
 * Copyright 2020 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ehrentreich.scrum.adapter.secondary.persistence

import arrow.core.Option
import net.ehrentreich.scrum.domain.Sprint
import net.ehrentreich.scrum.domain.SprintId
import net.ehrentreich.scrum.domain.SprintRepository
import org.hibernate.Session
import org.springframework.stereotype.Component
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Component
class JpaSprintRepository(@PersistenceContext private val entityManager: EntityManager) : SprintRepository {
    override fun add(sprint: Sprint) {
        val persistentSprint = PersistentSprint.fromDomainAggregate(sprint)
        entityManager.persist(persistentSprint)
    }

    override fun findById(sprintId: SprintId): Option<Sprint> {
        val maybePersistentSprint = entityManager
            .unwrap(Session::class.java)
            .bySimpleNaturalId(PersistentSprint::class.java)
            .load(sprintId)
        return Option
            .fromNullable(maybePersistentSprint)
            .map(PersistentSprint::toDomainAggregate)
    }
}
