/*
 * Copyright 2020 Marco Ehrentreich <marco@ehrentreich.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ehrentreich.scrum.adapter.primary.rest

import arrow.core.Either.Left
import arrow.core.Either.Right
import net.ehrentreich.scrum.adapter.primary.rest.SprintRestController.Companion.SPRINT_URI_PREFIX
import net.ehrentreich.scrum.application.ApplicationError
import net.ehrentreich.scrum.application.SprintApplicationService
import net.ehrentreich.scrum.application.SprintDto
import net.ehrentreich.scrum.application.SprintNotFoundError
import org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.util.*

@RequestMapping(SPRINT_URI_PREFIX)
@RestController
class SprintRestController(private val sprintApplicationService: SprintApplicationService) {
    companion object {
        const val SPRINT_URI_PREFIX = "/sprints"
    }

    @GetMapping("/{sprintId}", produces = [APPLICATION_JSON_VALUE])
    fun fetchSprintById(@PathVariable sprintId: UUID): ResponseEntity<SprintDto> {
        val eitherSprint = sprintApplicationService.fetchASingleSprintById(sprintId)
        return when (eitherSprint) {
            is Left<ApplicationError> ->
                when (eitherSprint.a) {
                    is SprintNotFoundError ->
                        ResponseEntity.notFound().build()
                }
            is Right<SprintDto> ->
                ResponseEntity.ok(eitherSprint.b)
        }
    }

    @PostMapping(produces = ["text/plain;charset=UTF-8"])
    fun createNewSprint(uriComponentsBuilder: UriComponentsBuilder): ResponseEntity<String> {
        val result = sprintApplicationService.createNewSprint()
        return when (result) {
            is Left<*> ->
                ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Error creating new sprint.")
            is Right<UUID> -> {
                val sprintId = result.b.toString()
                val locationUri = locationUriForSprint(uriComponentsBuilder, sprintId)
                ResponseEntity.created(locationUri).body("Created a new sprint at $locationUri")
            }
        }
    }

    private fun locationUriForSprint(uriComponentsBuilder: UriComponentsBuilder, sprintId: String): URI =
            MvcUriComponentsBuilder
                .fromController(uriComponentsBuilder, SprintRestController::class.java)
                .pathSegment(sprintId)
                .build()
                .toUri()
}
