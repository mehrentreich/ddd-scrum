# DDD Scrum

A simplified implementation of a Scrum collaboration tool as described in Vaughn Vernon's book "Domain-Driven Design Distilled" based on Kotlin with Spring Boot.

## About this project

The main purpose of this project is to experiment with specific technologies, ideas and concepts and ultimately to learn and gain more practical experience. Therefore some things which may seem unnecessarily complex or even completely over-engineered in this context may well be intended that way.

On the other hand some stuff like Kotlin is relatively new to me. So it's also possible that I am just plain wrong and have no clue :) If you have any criticism or suggestions please feel free to let me know and drop me a mail on marco@ehrentreich.net. Thanks in advance!
